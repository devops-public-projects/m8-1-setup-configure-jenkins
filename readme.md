
# Installing Jenkins
In this project we will run Jenkins on a DigitalOcean droplet as a Docker container.

## Technologies Used
- Jenkins
- Docker
- DigitalOcean
- Linux (Ubuntu)

## Project Description
- Create an Ubuntu server on DigitalOcean
- Setup and run Jenkins as Docker container
- Initialize Jenkins

## Prerequisites
- DigitalOcean Account

## Guide Steps
### Create a DigitalOcean Droplet Server
- We'll create a Droplet with 4GB of RAM, 80 GB SSD on a NY server or whichever is closest to you.
- We configure a firewall to allow port 22 (ssh) and 8080 (Jenkins)

### Install Docker
- `ssh root@JENKINS_SERVER_IP`
- `apt update`
- `apt install docker.io`

### Configure Jenkins Container
- `docker run -p 8080:8080 -p 50000:50000 -d -v jenkins_home:/var/jenkins_home jenkins/jenkins:lts`
	- This will configure ports 8080 (Jenkins) and 50000 (for cluster nodes)
	- `-d` for detached mode
	- `-v` for a named volume called `jenkins_home`
	- We'll use the latest official Jenkins image on Docker Hub

### Initialize Jenkins
- Navigate to `JENKINS_SERVER_IP:8080` in your web browser
- Get the temporary admin password from either of these two methods
	- Host Server 
		- `cat /var/lib/docker/volumes/jenkins_home/_data/secrets/initialAdminPassword` 
	- Inside the Jenkins Container
		- `docker exec -it container_id bash` 
		- `cat /var/jenkins_home/secrets/initialAdminPassword`
- Enter the password on the initialize screen
- Install Suggested Plugins (we can modify or install more later)
- Create your first admin account
	- username: chris
	- Set a password & confirm it, full name, and email
	- `Save & Continue`
	- `Save & Finish`

![Jenkins Successfully Initialized](/images/m8-1-jenkins-initialized.png)